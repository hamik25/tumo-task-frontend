import axios from 'axios';

const http = axios.create({
  baseURL: 'https://3296cd87.ngrok.io/api/',
  timeout: 3000
});

export default http;