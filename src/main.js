import Vue from 'vue';
import VueRouter from 'vue-router';

import App from './App.vue';
import JobsList from './JobsList';
import Job from './Job.vue';

Vue.use(VueRouter);

const routes = [
	{ path: '/', component: JobsList },
	{ path: '/jobs', component: JobsList },
	{ path: '/jobs/:jobId', component: Job }
];

const router = new VueRouter({
	routes: routes,
	mode: 'history'
});


const app = new Vue({
	el: '#app',
	router: router,
	template: '<App/>',
	components: { App },
})